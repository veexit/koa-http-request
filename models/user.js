let request = require('request');
module.exports = async function () {
    let user = await request('https://randomuser.me/api', function (error, response, body) {
        if (!error) {
            return body;
        } else {
            console.log('error:', error);
        }
    });
    return user;
};