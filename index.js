const Koa = require('koa');
const app = new Koa();

const Router = require('koa-router');
const router = new Router();


router.get('/user', require('./routes/user').get);

app.use(router.routes());
app.listen(3000);